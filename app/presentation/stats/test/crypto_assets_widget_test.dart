import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:stats/view/widgets/crypto_assets_states_widget.dart';

main() {
  const String kTrace = 'crypto_assets_widget_test';

  group('$kTrace WidgetTest:: ', () {
    testWidgets('Finds ListView in CryptoAssetsLoading',
        (WidgetTester tester) async {
      await tester.pumpWidget(const CryptoAssetsLoading());

      expect(find.byType(ListView), findsOneWidget);
    });

    testWidgets('Finds error msg in CryptoFailureView',
        (WidgetTester tester) async {
      final String findString = 'Failed to fetch assets';

      await tester.pumpWidget(const CryptoFailureView());

      expect(find.text(findString), findsOneWidget);
    });
  });
}
