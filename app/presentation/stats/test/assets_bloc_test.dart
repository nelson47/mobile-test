import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:stats/bloc/assets_bloc/assets_bloc.dart';

void main() {
  const String kTrace = 'assets_boc_test';

  group('$kTrace AssetsBloc:: ', () {
    late AssetsBloc assetsBloc;

    setUp(() {
      assetsBloc = AssetsBloc();
    });

    test('initial state equals AssetsInitial', () {
      expect(assetsBloc.state, AssetsInitial());
    });

    blocTest<AssetsBloc, AssetsState>(
      'should emit states when Fetching Assets',
      build: () => AssetsBloc(),
      act: (bloc) => bloc.add(FetchAssetsEvent()),
      expect: () => [AssetsLoading(), AssetsError()],
    );
  });
}
