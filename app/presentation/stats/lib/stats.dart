library stats;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stats/bloc/price_bloc/price_bloc.dart';

import 'bloc/assets_bloc/assets_bloc.dart';
import 'view/stats_page.dart';

class CryptoStatsPage extends StatelessWidget {
  const CryptoStatsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _initLocalDependencies();

    return MultiBlocProvider(
      providers: [
        BlocProvider<AssetsBloc>(
            create: (BuildContext context) => AssetsBloc()),
        BlocProvider<PriceBloc>(create: (BuildContext context) => PriceBloc()),
      ],
      child: StatsPage(),
    );
  }

  void _initLocalDependencies() {
    ///Add dependencies that are used within this Package
    ///Global dependencies should be created in Project's main.dart
  }
}
