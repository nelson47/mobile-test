part of 'assets_bloc.dart';

@immutable
abstract class AssetsState extends Equatable {
  const AssetsState();
}

class AssetsInitial extends AssetsState {
  @override
  List<Object> get props => [];
}

class AssetsLoading extends AssetsState {
  @override
  List<Object> get props => [];
}

class AssetsLoaded extends AssetsState {
  const AssetsLoaded();

  @override
  List<Object> get props => [];
}

class AssetsError extends AssetsState {
  @override
  List<Object> get props => [];
}
