import 'package:bloc/bloc.dart';
import 'package:core/core.dart'
    show CryptoAssetModel, Equatable, GetIt, CryptoAssets;
import 'package:meta/meta.dart';

part 'assets_event.dart';
part 'assets_state.dart';

class AssetsBloc extends Bloc<AssetsEvent, AssetsState> {
  AssetsBloc() : super(AssetsInitial()) {
    on<FetchAssetsEvent>(_fetchAssets);
  }
  List<CryptoAssetModel> assetsList = [];

  _fetchAssets(event, emit) async {
    emit(AssetsLoading());
    try {
      final _list = await GetIt.I.get<CryptoAssets>().fetch();
      assetsList = _list;
      emit(AssetsLoaded());
    } catch (_) {
      emit(AssetsError());
    }
  }

  CryptoAssetModel getAssetFromSupportedFilter(String filter) {
    return assetsList.where((element) => element.assetId == filter).first;
  }
}
