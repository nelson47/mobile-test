part of 'assets_bloc.dart';

@immutable
abstract class AssetsEvent extends Equatable {
  const AssetsEvent();
}

class FetchAssetsEvent extends AssetsEvent {
  const FetchAssetsEvent();

  @override
  List<Object> get props => [];
}
