part of 'price_bloc.dart';

@immutable
abstract class PriceEvent extends Equatable {
  const PriceEvent();
}

class FetchPriceEvent extends PriceEvent {
  const FetchPriceEvent();

  @override
  List<Object> get props => [];
}
