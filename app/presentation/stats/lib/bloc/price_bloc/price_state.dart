part of 'price_bloc.dart';

@immutable
abstract class PriceState extends Equatable {
  const PriceState();
}

class PriceInitial extends PriceState {
  @override
  List<Object> get props => [];
}

class PriceLoading extends PriceState {
  @override
  List<Object> get props => [];
}

class PriceLoaded extends PriceState {
  const PriceLoaded();

  @override
  List<Object> get props => [];
}

class PriceError extends PriceState {
  @override
  List<Object> get props => [];
}
