import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/core.dart';
import 'package:meta/meta.dart';

part 'price_event.dart';
part 'price_state.dart';

class PriceBloc extends Bloc<PriceEvent, PriceState> {
  PriceBloc() : super(PriceInitial()) {
    on<FetchPriceEvent>(_fetchPrice);
  }

  final _priceRepository = GetIt.I.get<CryptoPrice>();

  ///Model to listen and update other crypto supported streams
  StreamController<CryptoPriceModel> _currentCryptoModel =
      StreamController<CryptoPriceModel>();

  ///Seperate Streams for Supported Cryptos
  StreamController<CryptoPriceModel> bitStream = StreamController.broadcast();
  StreamController<CryptoPriceModel> ethStream = StreamController.broadcast();
  StreamController<CryptoPriceModel> adaStream = StreamController.broadcast();

  void _fetchPrice(event, emit) {
    emit(PriceLoading());
    try {
      final _cryptoStream = _priceRepository.fetch();
      _cryptoStream.listen((cryptoData) {
        final priceModel = _priceRepository.decodeData(cryptoData);

        _currentCryptoModel.add(priceModel);
      });

      _updateIndividualStreams();

      emit(PriceLoaded());
    } catch (_) {
      emit(PriceError());
    }
  }

  void _updateIndividualStreams() {
    _currentCryptoModel.stream.listen((cryptoPriceModel) {
      if (cryptoPriceModel.symbolId == btcFilter) {
        bitStream.add(cryptoPriceModel);
      } else if (cryptoPriceModel.symbolId == ethFilter) {
        ethStream.add(cryptoPriceModel);
      } else if (cryptoPriceModel.symbolId == adaFilter) {
        adaStream.add(cryptoPriceModel);
      }
    });
  }

  disposeStreams() {
    _currentCryptoModel.close();
    bitStream.close();
    ethStream.close();
    adaStream.close();
  }
}
