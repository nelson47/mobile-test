import 'package:flutter/material.dart';
import 'package:core/core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:stats/bloc/assets_bloc/assets_bloc.dart';
import 'package:stats/bloc/price_bloc/price_bloc.dart';
import 'widgets/crypto_assets_widget.dart';
import 'widgets/live_crypto_widget.dart';

class StatsPage extends StatefulWidget {
  const StatsPage({Key? key}) : super(key: key);

  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  late AppLocalizations l10n;
  late TextTheme textTheme;
  final cryptoAssets = GetIt.I.get<CryptoAssets>();
  final cryptoPrice = GetIt.I.get<CryptoPrice>();

  @override
  void initState() {
    context.read<AssetsBloc>().add(FetchAssetsEvent());
    context.read<PriceBloc>().add(FetchPriceEvent());
    super.initState();
  }

  @override
  void didChangeDependencies() {
    l10n = context.l10n;
    textTheme = Theme.of(context).textTheme;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: kToolbarHeight),
        child: NestedScrollView(
          headerSliverBuilder: (_, scrollInner) => [
            SliverToBoxAdapter(
              child: Column(
                children: [
                  _userWelcomeCard(),
                  const SizedBox(height: 20.0, width: 0.0),
                  _balanceAndProfitCard()
                ],
              ),
            ),
          ],
          body: SingleChildScrollView(
            child: Column(
              children: [
                _subHeadingText(l10n.livePrice),
                const SizedBox(height: 20.0, width: 0.0),
                const LiveCryptoWidget(),
                _subHeadingText(l10n.cryptoAssets),
                const SizedBox(height: 20.0, width: 0.0),
                const CryptoAssetsWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _subHeadingText(String text) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 15.0, top: 15.0),
        child: Text(
          text,
          style: textTheme.bodyText1,
        ),
      ),
    );
  }

  ListTile _userWelcomeCard() {
    return ListTile(
      title: Text(
        l10n.welcome,
        style: const TextStyle(
          fontSize: 16.0,
          color: Colors.black87,
          fontWeight: FontWeight.w500,
        ),
      ),
      subtitle: Text(l10n.userName, style: textTheme.subtitle1),
      trailing: const CircleAvatar(
        backgroundImage: AssetImage(AppImages.avatar),
        minRadius: 32.0,
        maxRadius: 32.0,
      ),
    );
  }

  Widget _balanceAndProfitCard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(25.0),
        child: Material(
          elevation: 20.0,
          color: AppColors.base,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                ListTile(
                  title: Text(l10n.balance, style: textTheme.bodyText2),
                  subtitle: Text(l10n.dummyBalance, style: textTheme.headline2),
                ),
                ListTile(
                  title: Text(l10n.monthlyProfit, style: textTheme.bodyText2),
                  subtitle:
                      Text(l10n.dummyMonthlyProfit, style: textTheme.headline3),
                  trailing: _dummyPercentage(10),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ClipRRect _dummyPercentage(int percentage) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(40.0),
      child: Container(
        color: Colors.white30,
        height: 30.0,
        width: 80.0,
        padding: const EdgeInsets.all(3.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.arrow_drop_up, color: Colors.greenAccent),
            Text('$percentage %', style: textTheme.overline)
          ],
        ),
      ),
    );
  }
}
