import 'package:flutter/material.dart';
import 'package:core/core.dart' show AppLocalizationsX, ContextSizeExtensions;
import 'package:shimmer/shimmer.dart';

class CryptoAssetsLoading extends StatelessWidget {
  const CryptoAssetsLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 7,
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      padding: EdgeInsets.zero,
      itemBuilder: (_, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15.0,
            vertical: 10.0,
          ),
          child: Shimmer.fromColors(
            baseColor: Colors.black45,
            highlightColor: Colors.black26,
            child: Container(
              width: context.percentWidth * 70,
              height: 40.0,
              color: Colors.black26,
            ),
          ),
        );
      },
    );
  }
}

class CryptoFailureView extends StatelessWidget {
  const CryptoFailureView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        context.l10n.assetsFailed,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }
}
