import 'package:flutter/material.dart';
import 'package:core/core.dart' show AppLocalizationsX, ContextSizeExtensions;
import 'package:shimmer/shimmer.dart';

class LiveCryptoLoading extends StatelessWidget {
  const LiveCryptoLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 15.0,
        vertical: 10.0,
      ),
      child: Shimmer.fromColors(
        baseColor: Colors.black45,
        highlightColor: Colors.black26,
        child: Container(
          width: context.percentWidth * 65,
          height: 80.0,
          color: Colors.black26,
        ),
      ),
    );
  }
}

class LiveCryptoFailureView extends StatelessWidget {
  const LiveCryptoFailureView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        context.l10n.assetsFailed,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }
}
