import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:core/core.dart';
import 'package:shimmer/shimmer.dart';

import 'package:stats/bloc/assets_bloc/assets_bloc.dart';
import 'package:stats/view/widgets/crypto_assets_states_widget.dart';

class CryptoAssetsWidget extends StatefulWidget {
  const CryptoAssetsWidget({Key? key}) : super(key: key);

  @override
  _CryptoAssetsWidgetState createState() => _CryptoAssetsWidgetState();
}

class _CryptoAssetsWidgetState extends State<CryptoAssetsWidget> {
  late AppLocalizations l10n;
  late TextTheme textTheme;

  @override
  void didChangeDependencies() {
    l10n = context.l10n;
    textTheme = Theme.of(context).textTheme;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: BlocBuilder<AssetsBloc, AssetsState>(
        builder: (context, state) {
          if (state is AssetsLoading) {
            return const CryptoAssetsLoading();
          }
          if (state is AssetsLoaded) {
            return _assetsList(context);
          }

          return CryptoFailureView();
        },
      ),
    );
  }

  ListView _assetsList(BuildContext context) {
    final _list = context.read<AssetsBloc>().assetsList;
    return ListView.builder(
      ///setting limit to 7 items
      itemCount: 7,
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      padding: EdgeInsets.zero,
      itemBuilder: (_, index) {
        return Card(
          child: SizedBox(
            width: context.percentWidth * 70,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15.0,
                vertical: 10.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _networkImage(_list[index].url),
                  Text(
                    _list[index].assetId,
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                    ),
                  ),
                  const SizedBox(height: 0.0, width: 0.0),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _networkImage(String url) {
    return CachedNetworkImage(
      imageUrl: url,
      height: 36.0,
      width: 36.0,
      placeholder: (context, url) => Shimmer.fromColors(
        baseColor: Colors.black45,
        highlightColor: Colors.black26,
        child: Container(
          height: 36.0,
          width: 36.0,
          color: Colors.black26,
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
