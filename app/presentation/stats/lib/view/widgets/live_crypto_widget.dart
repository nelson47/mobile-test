import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:core/core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

import 'package:stats/bloc/assets_bloc/assets_bloc.dart';
import 'package:stats/bloc/price_bloc/price_bloc.dart';
import 'live_crypto_states_widget.dart';

class LiveCryptoWidget extends StatelessWidget {
  const LiveCryptoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: context.percentHeight * 25,
      child: BlocBuilder<PriceBloc, PriceState>(
        builder: (context, state) {
          if (state is PriceLoading) {
            return const LiveCryptoLoading();
          }
          if (state is PriceLoaded) {
            return _PricingList();
          }

          return LiveCryptoFailureView();
        },
      ),
    );
  }
}

class _PricingList extends StatefulWidget {
  _PricingList({Key? key}) : super(key: key);

  @override
  _PricingListState createState() => _PricingListState();
}

class _PricingListState extends State<_PricingList> {
  late PriceBloc _priceBloc;
  late AssetsBloc _assetBloc;
  late AppLocalizations l10n;
  late TextTheme textTheme;
  late List<StreamController<CryptoPriceModel>> _listOfStreamControllers;

  @override
  void didChangeDependencies() {
    l10n = context.l10n;
    textTheme = Theme.of(context).textTheme;

    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _priceBloc = context.read<PriceBloc>();
    _assetBloc = context.read<AssetsBloc>();

    _listOfStreamControllers = [
      _priceBloc.bitStream,
      _priceBloc.ethStream,
      _priceBloc.adaStream,
    ];
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: _listOfStreamControllers.length,
      shrinkWrap: true,
      itemBuilder: (_, index) {
        return StreamBuilder<CryptoPriceModel>(
            stream: _listOfStreamControllers[index].stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return LiveCryptoLoading();
              }

              return _livePriceCard(context, snapshot.requireData);
            });
      },
    );
  }

  Padding _livePriceCard(BuildContext context, CryptoPriceModel _item) {
    final _assetModel = _assetBloc
        .getAssetFromSupportedFilter(supportedCryptosMap[_item.symbolId]!);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Card(
        child: SizedBox(
          width: context.percentWidth * 65,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 8.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    _networkImage(_assetModel.url),
                    const SizedBox(height: 0.0, width: 20.0),
                    Text(_assetModel.assetId, style: textTheme.headline4),
                    Text('/${l10n.usd}', style: textTheme.headline5)
                  ],
                ),
                Text('${_item.price}', style: textTheme.headline6),
                Text(l10n.sell, style: textTheme.caption),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _networkImage(String url) {
    return CachedNetworkImage(
      imageUrl: url,
      height: 52.0,
      width: 52.0,
      placeholder: (context, url) => Shimmer.fromColors(
        baseColor: Colors.black45,
        highlightColor: Colors.black26,
        child: Container(
          height: 52.0,
          width: 52.0,
          color: Colors.black26,
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  @override
  void dispose() {
    _priceBloc.disposeStreams();
    super.dispose();
  }
}
