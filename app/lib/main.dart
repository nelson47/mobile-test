import 'package:flutter/material.dart';
import 'package:core/core.dart';
import 'package:stats/stats.dart';

import 'package:crypto_stats/service_locator.dart';

void main() {
  ///Configure Service Locator
  ///!Don't forget to init Service Locator as local Package imports depend on it
  initServiceLocator(Env.prod);

// Run the app and pass its dependencies to it.
  runApp(const App());
}

/// {@template app}
/// The widget that handles the dependency injection of your application.
/// {@endtemplate}
class App extends StatelessWidget {
  /// {@macro app}
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const AppView();
  }
}

/// {@template app_view}
/// The widget that configures your application.
/// {@endtemplate}
class AppView extends StatelessWidget {
  /// {@macro app_view}
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      restorationScopeId: 'app',
      localizationsDelegates: localizationsDelegates,
      supportedLocales: supportedLocales,
      theme: appTheme,
      home: const CryptoStatsPage(),
    );
  }
}
