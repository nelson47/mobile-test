import 'package:core/core.dart';

///Instance of Service Locator
final serviceLocator = GetIt.instance;

///Init Dependencies and register based on [Env]
void initServiceLocator(String environment) {
  if (environment == Env.prod) {
    ///Register app-wide dependencies here
    _initRepos();
  }
  if (environment == Env.dev) {
    ///Register dependencies for dev Environment here
  }
}

void _initRepos() {
  serviceLocator.registerLazySingleton(() => CryptoAssets());
  serviceLocator.registerLazySingleton(() => CryptoPrice());
}
