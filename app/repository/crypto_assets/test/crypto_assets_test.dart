import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'package:crypto_assets/crypto_assets.dart';
import 'datasources/crypto_assets_mock_datasource.dart';
import 'mock_data/crypto_assets_mock_data.dart';
import 'package:crypto_assets/data/models/crypto_asset_model.dart';

void main() {
  const String kTrace = 'fetch_crypto_assets_test';

  final testDatasource = MockDatasource();

  setUp(() {
    when(
      testDatasource.fetch(),
    ).thenAnswer((_) => Future.value(mockCryptoAssetsList));
  });

  group('$kTrace CryptoAssets:: ', () {
    test('Mocked CryptoAssets - Success', () async {
      final response = await testDatasource.fetch();
      print(response);

      expect(response, isNotNull);
      expect(response, isA<List<Map<String, String>>>());
      expect(response.first, isA<Map<String, String>>());
      expect(response.first['asset_id'], 'BTC');
    });

    test('Live CryptoAssets - Success', () async {
      final _cryptoAssets = CryptoAssets();

      final response = await _cryptoAssets.fetch();
      print(response.first);

      expect(response, isNotNull);
      expect(response, isA<List<CryptoAssetModel>>());
      expect(response, isNotEmpty);
    });
  });
}
