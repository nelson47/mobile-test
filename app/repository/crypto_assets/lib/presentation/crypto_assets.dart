import 'package:core/core.dart' show Dio, Env, GetIt, NetworkDatasource;

import 'package:crypto_assets/data/datasources/crypto_asset_remote_datasource.dart';
import 'package:crypto_assets/data/models/crypto_asset_model.dart';
import 'package:crypto_assets/data/repositories/crypto_asset_repository_impl.dart';
import 'package:crypto_assets/domain/use_cases/crypto_assets_use_case.dart';

//ServiceLocator for CryptoAssets
GetIt _serviceLocator = GetIt.instance;

///Public Facing API For CryptoAssets
class CryptoAssets {
  CryptoAssets({
    ///Optional [Env]. Default is Env.prod
    String env = Env.prod,
  }) {
    ///Configure Service Locator
    _initServiceLocator(Env.prod);
  }

  _initServiceLocator(String env) {
    _serviceLocator.registerSingleton<Dio>(Dio());
    _serviceLocator.registerLazySingleton(() => CryptoAssetsUseCase());
    _serviceLocator.registerLazySingleton(() => CryptoAssetRepositoryImpl());

    if (env == Env.prod) {
      _serviceLocator.registerLazySingleton<NetworkDatasource>(
          () => CryptoAssetsRemoteDatasource());
    }
    if (env == Env.dev) {
      ///add dependencies for dev Environment
    }
  }

  Future<List<CryptoAssetModel>> fetch() async {
    final _cryptoRepository = _serviceLocator.get<CryptoAssetsUseCase>();
    return _cryptoRepository.fetch();
  }
}
