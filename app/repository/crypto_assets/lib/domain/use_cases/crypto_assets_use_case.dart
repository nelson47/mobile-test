import 'package:core/core.dart' show GetIt;

import 'package:crypto_assets/data/models/crypto_asset_model.dart';
import 'package:crypto_assets/data/repositories/crypto_asset_repository_impl.dart';

class CryptoAssetsUseCase {
  final _cryptoRepository = GetIt.I.get<CryptoAssetRepositoryImpl>();

  Future<List<CryptoAssetModel>> fetch() async =>
      await _cryptoRepository.fetch();
}
