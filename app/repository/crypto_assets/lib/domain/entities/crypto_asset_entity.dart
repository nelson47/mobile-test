import 'package:core/core.dart' show Equatable;

///Data Class For CryptoAsset
class CryptoAssetEntity extends Equatable {
  final String url;
  final String assetId;

  const CryptoAssetEntity({required this.assetId, required this.url});

  @override
  List<Object> get props => [assetId, url];
}
