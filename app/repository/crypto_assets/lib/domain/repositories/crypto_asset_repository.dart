import 'package:crypto_assets/data/models/crypto_asset_model.dart';

abstract class CryptoAssetRepository {
  Future<List<CryptoAssetModel>> fetch();
}