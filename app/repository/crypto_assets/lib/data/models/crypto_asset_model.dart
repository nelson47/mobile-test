import 'package:crypto_assets/domain/entities/crypto_asset_entity.dart';

/// Model Class for CryptoAsset
class CryptoAssetModel extends CryptoAssetEntity {
  final String assetId;
  final String url;

  const CryptoAssetModel({required this.assetId, required this.url})
      : super(assetId: assetId, url: url);

  factory CryptoAssetModel.fromJson(Map<String, dynamic> json) =>
      CryptoAssetModel(
          assetId: json['asset_id'] as String, url: json['url'] as String);
}
