import 'package:core/core.dart' show NetworkDatasource, GetIt;

import 'package:crypto_assets/data/models/crypto_asset_model.dart';
import 'package:crypto_assets/domain/repositories/crypto_asset_repository.dart';

class CryptoAssetRepositoryImpl extends CryptoAssetRepository {
  final _remoteSource = GetIt.I.get<NetworkDatasource>();

  @override
  Future<List<CryptoAssetModel>> fetch() async {
    final response = await _remoteSource.fetch();

    return _remoteSource.decode(response);
  }
}
