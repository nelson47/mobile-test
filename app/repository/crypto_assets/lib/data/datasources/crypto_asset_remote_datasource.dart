import 'package:flutter/foundation.dart';
import 'package:core/core.dart';

import 'package:crypto_assets/data/models/crypto_asset_model.dart';

/// Repository to fetch Crypto Assets from API
class CryptoAssetsRemoteDatasource implements NetworkDatasource {
  final _dioClient = GetIt.I.get<Dio>();

  ///Fetch and return dio.Response object
  @override
  Future<Response> fetch() async {
    late Response response;

    try {
      response = await _dioClient.get(
        '${CoinApi.restEndpoint}/assets/icons/100',
        options: Options(
            headers: <String, String>{'X-CoinAPI-Key': ApiKeys.coinAssetsApiKey}),
      );
    } on DioError catch (e) {
      //! Log in real app using a service e.g Sentry
      debugPrint('Trace - CryptoAssetsRemoteDatasource:: $e');
    }

    return response;
  }

  ///Decode the Response
  @override
  List<CryptoAssetModel> decode(Response response) {
    if (response.statusCode == 200) {
      final List<dynamic> responseData = response.data;

      final _list =
          responseData.map((data) => CryptoAssetModel.fromJson(data)).toList();

      return _list;
    }

    //return empty list on internal request failure
    return [];
  }
}
