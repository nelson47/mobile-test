import 'package:flutter_test/flutter_test.dart';

import 'package:crypto_price/presentation/crypto_price.dart';

void main() {
  const String kTrace = 'fetch_crypto_price_test';

  group('$kTrace CryptoPrice:: ', () {
    test('Live CryptoPrice - Success', () {
      final _cryptoAssets = CryptoPrice();

      final stream = _cryptoAssets.fetch();

      Future.delayed(Duration(seconds: 10));
      expect(stream, isNotNull);
      expect(stream, isA<Stream<dynamic>>());
    });
  });
}
