// ignore_for_file: prefer_single_quotes

const List<Map<String, String>> mockCryptoAssetsList = [
  {
    "asset_id": "BTC",
    "url":
        "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/4caf2b16a0174e26a3482cea69c34cba.png"
  },
  {
    "asset_id": "USD",
    "url":
        "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/0a4185f21a034a7cb866ba7076d8c73b.png"
  },
  {
    "asset_id": "PLN",
    "url":
        "https://s3.eu-central-1.amazonaws.com/bbxt-static-icons/type-id/png_512/3f682b5b77ec4d8cb612b8ff3ac748f7.png"
  },
];
