import 'dart:convert';

import 'package:core/core.dart' show GetIt;

import 'package:crypto_price/data/models/crypto_price_model.dart';
import 'package:crypto_price/data/repositories/crypto_price_repository_impl.dart';

class CryptoPriceUseCase {
  final _cryptoRepository = GetIt.I.get<CryptoPriceRepositoryImpl>();

  Stream fetch() => _cryptoRepository.fetch();

  ///[data] received from snapshot.requireData of StreamBuilder
  CryptoPriceModel decode(dynamic data) {
    final results = jsonDecode(data as String) as Map<String, dynamic>;
    return CryptoPriceModel.fromJson(results);
  }
}
