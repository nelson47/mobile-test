import 'package:core/core.dart' show Equatable;

///Data Class For CryptoPrice
class CryptoPriceEntity extends Equatable {
  final String timeExchange;
  final String timeCoinapi;
  final String uuid;
  final double price;
  final double size;
  final String takerSide;
  final String symbolId;
  final int sequence;
  final String type;

  CryptoPriceEntity(
      {required this.timeExchange,
      required this.timeCoinapi,
      required this.uuid,
      required this.price,
      required this.size,
      required this.takerSide,
      required this.symbolId,
      required this.sequence,
      required this.type});

  @override
  List<Object> get props => [
        timeExchange,
        timeCoinapi,
        uuid,
        price,
        size,
        takerSide,
        symbolId,
        sequence,
        type
      ];
}
