import 'package:core/core.dart' show GetIt;

import 'package:crypto_price/domain/repositories/crypto_price_repository.dart';
import 'package:crypto_price/data/datasources/crypto_price_remote_datasource.dart';

class CryptoPriceRepositoryImpl extends CryptoPriceRepository {
  final _remoteSource = GetIt.I.get<CryptoPriceRemoteDatasource>();

  @override
  Stream fetch() => _remoteSource.fetch();
}
