import 'dart:convert';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'package:core/core.dart';

/// Repository to fetch Crypto Assets from API
class CryptoPriceRemoteDatasource {
  ///Fetch and return dio.Response object
  Stream fetch() {
    final apiUri = Uri.parse(CoinApi.webSocket);

    final webSocket = WebSocketChannel.connect(apiUri);

    webSocket.sink.add(
      jsonEncode({
        'type': 'hello',
        'apikey': ApiKeys.coinPriceApiKey,
        'heartbeat': false,
        'subscribe_data_type': ['trade'],
        'subscribe_filter_symbol_id': supportedCryptoList
      }),
    );
    return webSocket.stream;
  }
}
