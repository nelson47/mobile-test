import 'package:crypto_price/domain/entities/crypto_price_entity.dart';

/// Model Class for CryptoPrice
class CryptoPriceModel extends CryptoPriceEntity {
  CryptoPriceModel(
      {required String timeExchange,
      required String timeCoinapi,
      required String uuid,
      required double price,
      required double size,
      required String takerSide,
      required String symbolId,
      required int sequence,
      required String type})
      : super(
            timeExchange: timeExchange,
            timeCoinapi: timeCoinapi,
            uuid: uuid,
            price: price,
            size: size,
            takerSide: takerSide,
            symbolId: symbolId,
            sequence: sequence,
            type: type);

  factory CryptoPriceModel.fromJson(Map<String, dynamic> json) =>
      CryptoPriceModel(
        timeExchange: json['time_exchange'] as String,
        timeCoinapi: json['time_coinapi'] as String,
        uuid: json['uuid'] as String,
        price: double.tryParse(json['price'].toString()) ?? 0.0,
        size: double.tryParse(json['size'].toString()) ?? 0.0,
        takerSide: json['taker_side'] as String,
        symbolId: json['symbol_id'] as String,
        sequence: int.tryParse(json['sequence'].toString()) ?? 0,
        type: json['type'] as String,
      );
}
