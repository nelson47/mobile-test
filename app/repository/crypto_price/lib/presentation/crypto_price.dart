import 'package:core/core.dart' show Env, GetIt;

import 'package:crypto_price/data/datasources/crypto_price_remote_datasource.dart';
import 'package:crypto_price/data/models/crypto_price_model.dart';
import 'package:crypto_price/data/repositories/crypto_price_repository_impl.dart';
import 'package:crypto_price/domain/use_cases/crypto_price_use_case.dart';

//ServiceLocator for CryptoAssets
GetIt _serviceLocator = GetIt.instance;

///Public Facing API For CryptoAssets
class CryptoPrice {
  CryptoPrice({
    ///Optional [Env]. Default is Env.prod
    String env = Env.prod,
  }) {
    ///Configure Service Locator
    _initServiceLocator(Env.prod);
  }

  _initServiceLocator(String env) {
    _serviceLocator.registerLazySingleton(() => CryptoPriceUseCase());
    _serviceLocator.registerLazySingleton(() => CryptoPriceRepositoryImpl());

    if (env == Env.prod) {
      _serviceLocator
          .registerLazySingleton(() => CryptoPriceRemoteDatasource());
    }
    if (env == Env.dev) {
      ///add dependencies for dev Environment
    }
  }

  Stream fetch() {
    final _cryptoRepository = _serviceLocator.get<CryptoPriceUseCase>();

    return _cryptoRepository.fetch();
  }

  ///[data] received from snapshot.requireData of StreamBuilder
  CryptoPriceModel decodeData(dynamic data) {
    final _cryptoRepository = _serviceLocator.get<CryptoPriceUseCase>();
    return _cryptoRepository.decode(data);
  }
}
