import 'package:flutter/material.dart';

class AppColors {
  static const Color base = const Color(0xff516dfb);

  static const MaterialColor swatch = const MaterialColor(
    0xff516dfb,
    const <int, Color>{
      50: const Color(0xff4962e2),
      100: const Color(0xff4157c9),
      200: const Color(0xff314197),
      300: const Color(0xff29377e),
      400: const Color(0xff202c64),
      500: const Color(0xff18214b),
      600: const Color(0xff18214b),
      700: const Color(0xff101632),
      800: const Color(0xff080b19),
      900: const Color(0xff000000),
    },
  );
}
