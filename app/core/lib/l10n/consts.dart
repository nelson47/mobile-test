import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:core/l10n/arb/app_localizations.dart';


//App supported locales used in main.dart
Iterable<Locale> supportedLocales = const [
  Locale('en', ''),
];

//localizationsDelegates used in main.dart
const Iterable<LocalizationsDelegate<dynamic>>? localizationsDelegates = [
  AppLocalizations.delegate,
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  GlobalCupertinoLocalizations.delegate,
];
