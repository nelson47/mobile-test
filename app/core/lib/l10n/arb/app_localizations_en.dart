import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get welcome => 'Welcome';

  @override
  String get userName => 'Nelson John';

  @override
  String get balance => 'Balance';

  @override
  String get dummyBalance => '\$450,933';

  @override
  String get monthlyProfit => 'Monthly Profit';

  @override
  String get dummyMonthlyProfit => '\$12,484';

  @override
  String get dummyPercentage => '10 %';

  @override
  String get livePrice => 'Live Price';

  @override
  String get cryptoAssets => 'Crypto Assets';

  @override
  String get sell => 'SELL';

  @override
  String get usd => 'USD';

  @override
  String get btc => 'BTC';

  @override
  String get eth => 'ETH';

  @override
  String get ada => 'ADA';

  @override
  String get assetsFailed => 'Failed to fetch assets';

  @override
  String get pricesFailed => 'Failed to fetch Crypto prices';
}
