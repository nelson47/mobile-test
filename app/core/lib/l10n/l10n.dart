import 'package:flutter/material.dart';

import 'package:core/l10n/arb/app_localizations.dart';

/// Extension methods for `AppLocalizations` using `BuildContext`.
extension AppLocalizationsX on BuildContext {
  /// Short-hand for `AppLocalizations.of(context)`.
  AppLocalizations get l10n => AppLocalizations.of(this);
}
