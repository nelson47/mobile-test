library core;

//Local Packages
export 'package:crypto_assets/crypto_assets.dart';
export 'package:crypto_price/crypto_price.dart';
export 'package:crypto_price/data/models/crypto_price_model.dart';

//Packages
export 'package:dio/dio.dart';
export 'package:get_it/get_it.dart';
export 'package:equatable/equatable.dart';

//Abstract
export 'abstract/network_datasource.dart';

//L10n
export 'l10n/l10n.dart';
export 'l10n/arb/app_localizations.dart';
export 'l10n/consts.dart';

//Utils
export 'util/app_sizes.dart';
export 'util/app_colors.dart';
export 'util/app_env.dart';

//Consts
export 'consts/app_assets.dart';
export 'consts/app_themes.dart';
export 'consts/keys.dart';
export 'consts/endpoints.dart';
export 'consts/supported_crypto.dart';
