const String btcFilter = 'COINBASE_SPOT_BTC_USD';
const String ethFilter = 'COINBASE_SPOT_ETH_USD';
const String adaFilter = 'COINBASE_SPOT_ADA_USD';

///List for supported Crypto Currencies for Coin API
const List<String> supportedCryptoList = [
  r'COINBASE_SPOT_BTC_USD$',
  r'COINBASE_SPOT_ETH_USD$',
  r'COINBASE_SPOT_ADA_USD$'
];

const Map<String, String> supportedCryptosMap = {
  btcFilter: 'BTC',
  adaFilter: 'ADA',
  ethFilter: 'ETH'
};
