class FontFamily {
  FontFamily._();

  static const String poppins = 'Poppins';
}

class AppImages {
  ///Base location for image assets
  static const String base = 'assets/images';
  static const String avatar = '$base/avatar.jpeg';
  static const String btc = '$base/btc.png';
  static const String eth = '$base/eth.png';
  static const String ada = '$base/ada.png';
}
