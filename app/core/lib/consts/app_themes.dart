import 'package:flutter/material.dart';

import 'package:core/consts/app_assets.dart';
import 'package:core/util/app_colors.dart';

ThemeData appTheme = ThemeData(
  fontFamily: FontFamily.poppins,
  primaryColor: AppColors.base,
  primarySwatch: AppColors.swatch,
  textTheme: const TextTheme(
    headline1: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
    headline2: TextStyle(
      fontSize: 32.0,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle(
      fontSize: 30.0,
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
    headline4: TextStyle(
      fontSize: 24.0,
      fontWeight: FontWeight.w600,
      color: Colors.black,
    ),
    headline5: TextStyle(
      fontSize: 24.0,
      fontWeight: FontWeight.w400,
      color: Colors.black54,
    ),
    headline6: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
    bodyText1: TextStyle(fontSize: 18.0, color: Colors.black87),
    bodyText2: TextStyle(fontSize: 18.0, color: Colors.white),
    subtitle1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
    subtitle2: TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    ),
    caption: TextStyle(fontSize: 17.0, color: Colors.black),
    overline: TextStyle(fontSize: 16.0, color: Colors.white),
  ),
);
