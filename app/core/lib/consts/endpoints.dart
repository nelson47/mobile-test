///Remote API Endpoints for CoinAPI
class CoinApi {
  ///REST
  static const String restEndpoint = 'https://rest.coinapi.io/v1';

  ///WebSocket
  static const String webSocket = 'wss://ws-sandbox.coinapi.io/v1/';
}
