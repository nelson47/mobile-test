import 'package:dio/dio.dart';

///abstract class for http requests and decode json
abstract class NetworkDatasource {
  Future<dynamic> fetch();
  dynamic decode(Response response);
}
