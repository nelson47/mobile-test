**Please Open README in Online or Desktop Editor to view the Structure**
# mobile-app-test by Nelson John

Hi, given that this project was to test how I solve this problem, I took the liberty of removing some imports and restructure the project.

## Main Components:
- BLoC : State Manager
- GetIt : Service Locator
- Architecture: Clean Architecture + Modular

## Architecture: 
The Architecture is a combination of Clean Architecture(purposed by Robert C. Martin) and a Modular Structure.

Each Module is a Dart/Flutter Package. The thought process is to decouple as much as possible. This way multiple developers can work on their own 
Module/Package which can then later be imported into the main app via adding dependency in pubspec.yaml. 

I have been using this architecture with GetX in the past and it supports binding routes with Dependencies out of the box, a feature that i really appreciate due to my C#
background. Implementing it with BLoC was quite interesting and it may further be improved.

-**App**
  |
  |_**core**: A key Package/Modules that contains all the common packages, consts, helpers, routes, l10n etc.
  |      |_consts
  |      |_l10n
  |      |_utils/helpers
  |_**assets**: Directory with app assets (I took the liberty of removing generator for assets)
  |
  |_**presentation**: Contains all the  UI Packages. All the Features/Pages/UI exist under this directory as independent packages each with their own BLoC/Viewmodels
  |             |_ Stats : Includes main UI and Business logic for the app
  |             |       |_BLoC
  |             |       |_View
  |             |       |_test : each Module can have it's own tests
  |             |
  |             |_ Feature A
  |             |_ Feature B
  |
  |_**repository**: Or as some also call network directory. It contains the main components for Clean Architecture. Each Feature in Presentation can have it's own Repository Package
  |             so multiple developers can work on differnet Modules
  |            |_ crypto_assets:
  |            |              |_data
  |            |              |_domain
  |            |              |_presentation: Presentation in repository's case is only a wrapper to expose Public API as there wont be any UI here
  |            |              |_test : each Module can have it's own tests
  |            |_ crypto_price:
  |                          |_data
  |                          |_domain
  |                          |_presentation
  |                          |_test 
  |
  |_**lib**:
        |_main.dart : entry point for the app and to initialize service_locator
        |_service_locator.dart
        |_pubspec.yaml : to import all the local packages and stitch the app together


## Tests: 
The following tests can be found under respective directories.
- Unit Test: 
           |_app/repository/crypto_assets/test
           |_app/repository/crypto_price/test
             
- BLoC Test: app/presentation/stats/test/assets_bloc_test.dart
- Widget Test: app/presentation/stats/test/crypto_assets_widget_test.dart


Please reach out anytime if you need further description. Thanks.
Nelson John

Email: nelson_john@icloud.com
in: http://linkedin.com/in/solobits/
